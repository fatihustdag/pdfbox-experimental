package com.es.pdfbox.pdfboxexperimental.service;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.util.Matrix;
import org.springframework.stereotype.Service;

/**
 * Created by f.ustdag on 02.28.2019
 */
@Service
public class PDFBoxService
{


    private final String path ="C:/PdfBox_Examples";

   //Extract Text From PDF File
    public String extractTextFromPDF(final String filename) throws IOException
    {
        PDDocument  pdd =  PDDocument.load(new File(filename));
        PDFTextStripper pdfStripper = new PDFTextStripper();
        return pdfStripper.getText(pdd);
    }

    //Create PDF File
    public void createPDF(final String filename) throws IOException
    {
        PDDocument document = new PDDocument();
        document.save("C:/PdfBox_Examples/"+filename+".pdf");
        document.close();
    }

    //Add Page To PDF File
    public void addPageToPDF(final String filename) throws IOException
    {
        PDDocument document = new PDDocument();
        for (int i=0; i<10; i++) {
            //Prepare Blank Document
            PDPage pageToAdd = new PDPage();
            //Add Blank Page To PDF Document
            document.addPage(pageToAdd);
        }
        document.save("C:/PdfBox_Examples/"+filename+".pdf");
        document.close();
    }

    //Remove Second Page From PDF
    public void removePageFromPDF(final String filePath,final int pageToRemoved) throws IOException
    {
        String realPath = path +"/" + filePath +".pdf";
        File file = new File(realPath);
        PDDocument  pdd =  PDDocument.load(file);
        int noOfPages= pdd.getNumberOfPages();
        System.out.print(noOfPages);
        pdd.removePage(pageToRemoved);
        pdd.save("C:/PdfBox_Examples/"+filePath+".pdf");
        pdd.close();
    }

    //Split PDF Documents
    public void splitPDFDocument(final String filePath) throws IOException
    {

        String realPath = path +"/" + filePath +".pdf";
        File file = new File(realPath);
        PDDocument  document =  PDDocument.load(file);
        //Instantiating Splitter class
        Splitter splitter = new Splitter();

        //splitting the pages of a PDF document
        List<PDDocument> Pages = splitter.split(document);

        //Creating an iterator
        Iterator<PDDocument> iterator = Pages.listIterator();

        //Saving each page as an individual document
        int i = 1;
        while(iterator.hasNext()) {
            PDDocument pd = iterator.next();
            pd.save("C:/PdfBox_Examples/"+ i++ +".pdf");
        }
        System.out.println("Multiple PDF’s created");
        document.close();
    }

    //Merge PDF Files
    public void mergePDFDocuments(final String filePath1, final String filePath2) throws IOException
    {
        String realPathFirst= path +"/" + filePath1 +".pdf";

        String realPathSecond= path +"/" + filePath2 +".pdf";

        File file1 = new File(realPathFirst);
        PDDocument doc1 = PDDocument.load(file1);

        File file2 = new File(realPathSecond);
        PDDocument doc2 = PDDocument.load(file2);

        //Instantiating PDFMergerUtility class
        PDFMergerUtility PDFmerger = new PDFMergerUtility();

        //Setting the destination file
        PDFmerger.setDestinationFileName("C:/PdfBox_Examples/merged.pdf");

        //adding the source files
        PDFmerger.addSource(file1);
        PDFmerger.addSource(file2);

        //Merging the two documents
        PDFmerger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
        System.out.println("Documents merged");
        //Closing the documents
        doc1.close();
        doc2.close();

    }

    //Crop PDF File
    public void cropPDF (final String filePath1) throws IOException {
        String realPathFirst= path +"/" + filePath1 +".pdf";
        File file1 = new File(realPathFirst);
        PDDocument doc = PDDocument.load(file1);
        PDPage page = doc.getPage(0);
        page.setCropBox(new PDRectangle(40f, 680f, 510f, 100f));

        doc.save("C:/PdfBox_Examples/"+"croppedPage"+".pdf");
        doc.close();
    }

    //Rotate Page Of PDF
    public void rotatePDF (final String filePath1) throws IOException {
        String realPathFirst= path +"/" + filePath1 +".pdf";
        File file1 = new File(realPathFirst);
        PDDocument doc = PDDocument.load(file1);
        PDPage page = doc.getDocumentCatalog().getPages().get(0);
        page.setCropBox(new PDRectangle(40f, 680f, 510f, 100f));
        PDPageContentStream cs = new PDPageContentStream(doc, page, PDPageContentStream.AppendMode.PREPEND, false, false);
        cs.transform(Matrix.getRotateInstance(Math.toRadians(45), 0, 0));
        cs.close();
        doc.save(new File("C:/PdfBox_Examples/", "roteatedPage.pdf"));
        doc.close();
    }

    //Move Page To Last Position
    public void movePDF (final String filePath1) throws IOException {
        String realPathFirst= path +"/" + filePath1 +".pdf";
        File file1 = new File(realPathFirst);
        PDDocument doc = PDDocument.load(file1);
        PDPageTree allPages = doc.getDocumentCatalog().getPages();

        if (allPages.getCount() > 1) {
            PDPage lastPage = allPages.get(allPages.getCount() - 1);
            allPages.remove(allPages.getCount() - 1);
            PDPage firstPage = allPages.get(0);
            allPages.insertBefore(lastPage, firstPage);
        }
        doc.save(new File("C:/PdfBox_Examples/", "reOrderedPage.pdf"));
        doc.close();
    }


    //Replace page with blank page
    public void replacePDFPage (final String path1,final int pageToReplace) throws IOException {
        String realPathFirst= path +"/" + path1 +".pdf";
        File file1 = new File(realPathFirst);
        PDDocument a1doc = PDDocument.load(file1);
        PDDocument resDoc = new PDDocument();
        Iterator<PDPage> a1Pages = a1doc.getDocumentCatalog().getPages().iterator();
        int i = 1;
        while(a1Pages.hasNext()) {
            PDPage pd = a1Pages.next();

            if (i == pageToReplace) {
                PDPage pageToAdd = new PDPage();
                resDoc.addPage(pageToAdd);
            }
            else {
                resDoc.addPage(pd);
            }

            i++;
        }
        resDoc.save(new File("C:/PdfBox_Examples/", "replacedPage.pdf"));
        a1doc.close();
        resDoc.close();

    }


    //Remove Sensitive Information with predefined coordination with image which send with parameters
    public void removeSensitiveInformation (final String path1, final String imagePath) throws IOException {
        String realPathFirst= path +"/" + path1 +".pdf";
        File file1 = new File(realPathFirst);

        PDDocument document= PDDocument.load( file1);

        PDPage page = document.getPage(0);

        String realPathImage= path +"/" + imagePath +".png";

        // createFromFile is the easiest way with an image file
        // if you already have the image in a BufferedImage,
        // call LosslessFactory.createFromImage() instead
        PDImageXObject pdImage = PDImageXObject.createFromFile(realPathImage, document);
        PDPageContentStream contentStream = new PDPageContentStream(document, page, true, true);

        // contentStream.drawImage(ximage, 20, 20 );
        // better method inspired by http://stackoverflow.com/a/22318681/535646
        // reduce this value if the image is too large
        float scale = 1f;
        contentStream.drawImage(pdImage, 20, 20, pdImage.getWidth()*scale, pdImage.getHeight()*scale);

        contentStream.close();
        document.save( new File("C:/PdfBox_Examples/", "redactionedPage.pdf") );
        document.close();
    }
























}
