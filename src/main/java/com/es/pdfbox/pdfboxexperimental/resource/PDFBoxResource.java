package com.es.pdfbox.pdfboxexperimental.resource;

import java.io.IOException;

import com.es.pdfbox.pdfboxexperimental.service.PDFBoxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by f.ustdag on 02.28.2019.
 */
@RestController
class PDFBoxResource
{
    private static final Logger logger = LoggerFactory.getLogger(PDFBoxResource.class);

   @Autowired
   private PDFBoxService pdfbox;


   @RequestMapping(value = "/v1/pdf/extract", method = RequestMethod.GET)
   @ResponseStatus(value = HttpStatus.OK)
   public ResponseEntity extractText(@PathVariable final String filename){
       try {
            String extractedText = pdfbox.extractTextFromPDF(filename);
            logger.info(extractedText);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
   }

    @GetMapping("/pdf/createPDF/{fileName}")
    public ResponseEntity createPDF(@PathVariable("fileName") String filename){
        try {
            pdfbox.createPDF(filename);
            return new ResponseEntity(HttpStatus.OK);

        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }



    @GetMapping("/pdf/addPageToPDF/{fileName}")
    public ResponseEntity addPageToPdf(@PathVariable("fileName") final String filename){
        try {
            pdfbox.addPageToPDF(filename);
            return new ResponseEntity(HttpStatus.OK);

        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/pdf/removePageFromPDF/{fileName}/{pageToRemoved}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity removePageFromPDF(@PathVariable("fileName") final String filename,@PathVariable("pageToRemoved") final int pageToRemove){
        try {
            pdfbox.removePageFromPDF(filename,pageToRemove);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/pdf/splitPDFDocument/{fileName}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity splitPDFDocument(@PathVariable("fileName") final String filename){
        try {
            pdfbox.splitPDFDocument(filename);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/pdf/mergePDFDocuments/{fileName}/{fileNameSecond}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity mergePDFDocuments(@PathVariable("fileName") final String filename,@PathVariable("fileNameSecond") final String fileName2){
            try {
            pdfbox.mergePDFDocuments(filename,fileName2);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/pdf/cropPDF/{fileName}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity cropPDF(@PathVariable("fileName") final String filename){
        try {
            pdfbox.cropPDF(filename);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/pdf/rotatePDF/{fileName}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity rotatePDF(@PathVariable("fileName") final String filename){
        try {
            pdfbox.rotatePDF(filename);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/pdf/movePDF/{fileName}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity movePDF(@PathVariable("fileName") final String filename){
        try {
            pdfbox.movePDF(filename);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }



    @RequestMapping(value = "/pdf/replacePDFPage/{fileName}/{page}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity replacePDFPage(@PathVariable("fileName") final String filename, @PathVariable("page") final int page){
        try {
            pdfbox.replacePDFPage(filename,page);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/pdf/removeSensitive/{fileName}/{imagePath}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity removeSensitiveInformation(@PathVariable("fileName") final String filename, @PathVariable("imagePath") final String imagePath){
        try {
            pdfbox.removeSensitiveInformation(filename,imagePath);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (IOException e){
            logger.error(e.toString());
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


}
