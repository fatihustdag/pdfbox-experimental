package com.es.pdfbox.pdfboxexperimental.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by f.ustdag on 01.03.2019.
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.es.pdfbox.pdfboxexperimental")
public class Application
{


    public static void main(String[] args)
    { SpringApplication.run(Application.class, args);


    }



}
